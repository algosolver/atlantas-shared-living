const suiteCardData = [
  {
    image: "/Images/suit/presidential_suite.webp",
    link: "/our-suites/presidential",
    suitName: "presidential suite",
  },
  {
    image: "/Images/suit/governor_suite.jpg",
    link: "/our-suites/governor",
    suitName: "governor's suite",
  },

  {
    image: "/Images/suit/mayor_suite.jpg",
    link: "/our-suites/mayor",
    suitName: "mayor's suite",
  },

  {
    image: "/Images/suit/executive_suit.jpg",
    link: "/our-suites/executive",
    suitName: "executive suite",
  },
];

export default suiteCardData;
