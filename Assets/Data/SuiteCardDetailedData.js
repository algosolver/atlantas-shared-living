const suiteCardDetailedData = [
  {
    image: "/Images/suite/presidential_suite.webp",
    link: "/our-suites/presidential",
    suitName: "presidential suite",
    price: 44.65,
  },
  {
    image: "/Images/suite/governor_suite.jpg",
    link: "/our-suites/governor",
    suitName: "governor's suite",
    price: 25,
  },

  {
    image: "/Images/suite/mayor_suite.jpg",
    link: "/our-suites/mayor",
    suitName: "mayor's suite",
    price: 23.95,
  },

  {
    image:
      "/Images/bottom_suite_image/exe2.jpg",
    link: "/our-suites/executive",
    suitName: "executive suite",
    price: 19.73,
  },
];

export default suiteCardDetailedData;
