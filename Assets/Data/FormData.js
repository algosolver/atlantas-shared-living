import SelectMenu from "../../components/SelectMenu";

export const location_options = [
  {
    value: "buckhead",
    label: "Buckhead",
    property: [{ value: 0, label: "No property available right now" }],
  },
  {
    value: "decatur",
    label: "Decatur",
    property: [
      {
        value: 0,
        label: "Suite 750, W Ct Square, Decatur, GA 30030, United States",
      },
    ],
  },
  {
    value: "atlanta",
    label: "Atlanta",
    property: [{ value: 0, label: "No property available right now" }],
  },
  {
    value: "alpharetta",
    label: "Alpharetta",
    property: [{ value: 0, label: "No property available right now" }],
  },
];

export const suite_options = [
  {
    value: "presidential",
    label: "Presidential Suite",
    max_adult: 2,
    max_children: 2,
    beds: [
      {
        value: 1,
        label: "1",
      },
    ],
  },
  {
    value: "governor",
    label: "Governor's Suite",
    max_adult: 2,
    max_children: 3,

    beds: [
      {
        value: 1,
        label: "1",
      },
      { value: 2, label: "2" },
    ],
  },
  {
    value: "mayor",
    label: "Mayor's Suite",
    max_adult: 4,
    max_children: 4,

    beds: [
      {
        value: 1,
        label: <SelectMenu bedName="C1- Left Bottom" />,
      },
      { value: 2, label: "C2- Left Top" },
      {
        value: 3,
        label: "C3- Right Top",
      },
      {
        value: 4,
        label: <SelectMenu bedName="C4- Right Bottom" />,
      },
    ],
  },
  {
    value: "executive",
    label: "Executive Suite",
    max_adult: 4,
    max_children: 10,
    beds: [
      {
        value: 1,
        label: <SelectMenu bedName="C1- Left Bottom" />,
      },
      { value: 2, label: "C2- Left Middle" },
      {
        value: 3,
        label: "C3- Left Top",
      },
      {
        value: 4,
        label: 'C4- Middle Top',
      },
      {
        value: 5,
        label: 'C5- Middle Middle',
      },
      {
        value: 6,
        label: <SelectMenu bedName="C6- Middle Bottom" />,
      },
      {
        value: 7,
        label: <SelectMenu bedName="C7- Right Bottom" />,
      },
      {
        value: 8,
        label:'C8- Right Middle',
      },
      {
        value: 9,
        label: 'C9- Right Top'
      }
    ],
  },
];
