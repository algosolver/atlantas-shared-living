import {
  presedentialCaresoul,
  governorCaresoul,
  mayorCaresoul,
  executiveCaresoul,
} from "./SingleCaresoul";

export const PresidentialFacilities = [
  {
    slug: "presidential",
    title: "PRESIDENTIAL SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f957.svg",
    facility: "Max 2 person",
    amount: 44.65,
  },
  {
    slug: "presidential",
    title: "PRESIDENTIAL SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f4b0.svg",
    facility: "Free cancellation",
    amount: 50,
  },
  {
    slug: "executive",
    title: "EXECUTIVE'S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f697.svg",
    facility: "Parking included",
    amount: 150,
  },
];

export const governorFacilities = [
  {
    slug: "governor",
    title: "GOVERNOR’S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f957.svg",
    facility: "Max 2 person",
    amount: 25,
  },
  {
    slug: "governor",
    title: "GOVERNOR’S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f4b0.svg",
    facility: "Free cancellation",
    amount: 25,
  },
  {
    slug: "mayor",
    title: "MAYOR’S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f697.svg",
    facility: "Parking included",
    amount: 25,
  },
];

export const mayorsFacilities = [
  {
    slug: "mayor",
    title: "MAYOR’S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f957.svg",
    facility: "Max 6 person (2 story bunk bed- 3 pairs)",
    amount: 23.95,
  },
  {
    slug: "mayor",
    title: "MAYOR’S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f4b0.svg",
    facility: "Free cancellation",
    amount: 120,
  },
 
];

export const executiveFacilities = [
  {
    slug: "executive",
    title: "EXECUTIVE'S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f957.svg",
    facility: "Max 9 person (3 story bunk bed- 3 pairs)",
    amount: 19.73,
  },
  {
    slug: "executive",
    title: "EXECUTIVE'S SUITE",
    image: "https://s.w.org/images/core/emoji/13.0.1/svg/1f4b0.svg",
    facility: "Free cancellation",
    amount: 150,
  },
  
];

export const PresidentialSuitDetails = [
  {
    slug: "presidential",
    title: "PRESIDENTIAL SUITE",
    carouselImages: presedentialCaresoul,
    price: 50,
    desc: "Blend in and make a new friend in this shared CO’ED Executive Suite. Perfect for traveling bands, music artists groups, sports teams etc… This area is the Master bedroom and the finished basement. Enjoy our Bronze Plus Amenities:",
    reviews: [
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
    ],
    features: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
    ],
    tabSlug: "property",
  },
];

export const governorSuitDetails = [
  {
    slug: "governor",
    title: "GOVERNOR’S SUITE",
    carouselImages: governorCaresoul,
    price: 90,
    desc: "Blend in and make a new friend in this shared CO’ED Executive Suite. Perfect for traveling bands, music artists groups, sports teams etc… This area is the Master bedroom and the finished basement. Enjoy our Bronze Plus Amenities:",
    reviews: [
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
    ],
    features: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
    ],
    tabSlug: "direction",
  },
];

export const mayorsSuitDetails = [
  {
    slug: "mayor",
    title: "MAYOR’S SUITE",
    carouselImages: mayorCaresoul,
    price: 120,
    desc: "Blend in and make a new friend in this shared CO’ED Executive Suite. Perfect for traveling bands, music artists groups, sports teams etc… This area is the Master bedroom and the finished basement. Enjoy our Bronze Plus Amenities:",
    reviews: [
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
    ],
    features: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
    ],
    tabSlug: "useful",
  },
];

export const executiveSuitDetails = [
  {
    slug: "executive",
    title: "EXECUTIVE'S SUITE",
    carouselImages: executiveCaresoul,
    price: 120,
    desc: "Blend in and make a new friend in this shared CO’ED Executive Suite. Perfect for traveling bands, music artists groups, sports teams etc… This area is the Master bedroom and the finished basement. Enjoy our Bronze Plus Amenities:",
    reviews: [
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
      {
        reviewerName: "Jose Reeves",
        rating: "5",
        date: "01/27/2019",
        comment:
          "Clean and comfortable. Very clean and great amenities.. Great customer service and again, very welcoming. Helped with baggage to the room and explained where everything was before leaving.. Wifi was strong. No issue at all, which was important to me for the stay.",
      },
    ],
    features: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim",
    ],
    tabSlug: "property",
  },
];

export const allSuitsDetails = [
  ...PresidentialSuitDetails,
  ...governorSuitDetails,
  ...mayorsSuitDetails,
  ...executiveSuitDetails,
];
