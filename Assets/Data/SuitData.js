import {
  PresidentialFacilities,
  governorFacilities,
  mayorsFacilities,
  executiveFacilities,
} from "./Facilities";

export const suitData = [
  {
    roomImage: "/Images/our-suites/presidential.webp",
    roomFacilities: PresidentialFacilities,
    orientation: "RL",
  },
  {
    roomImage: "/Images/our-suites/governor.webp",
    roomFacilities: governorFacilities,
    orientation: "LR",
  },
  {
    roomImage: "/Images/our-suites/mayor.jpg",
    roomFacilities: mayorsFacilities,
    orientation: "RL",
  },
  {
    roomImage: "/Images/our-suites/executive.jpg",
    roomFacilities: executiveFacilities,
    orientation: "LR",
  },
];
