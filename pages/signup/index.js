import React from "react";
import HomeLayout from "../../leyouts/HomeLayout";
import SignUp from '../../components/SubPages/SignUp';
import HeaderHero from "../../components/Hero/HeaderHero";

index.getInitialProps = async ({ query }) => {
  return {query}
}

function index({query}) {
  return (
    <div>
      <HomeLayout>
        <HeaderHero
          text="Sign Up"
          image="/Images/heroBG.jpg"
        />
        <SignUp query={query} />
      </HomeLayout>
    </div>
  );
}

export default index;
