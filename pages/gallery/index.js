import React from "react";
import SubGallerySection from "../../components/SubPages/Gallery/SubGallerySection";
import SubHeroSection from "../../components/SubPages/Gallery/SubHeroSection"

import HomeLayout from "../../leyouts/HomeLayout";

const index = () => {
  return (
    <HomeLayout>
        <SubHeroSection/>
      <SubGallerySection />
    </HomeLayout>
  );
};

export default index;
