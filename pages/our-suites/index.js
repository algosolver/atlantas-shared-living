import { suitData } from "../../Assets/Data/SuitData";
import SubSuites from "../../components/SubPages/OurSuits/SubSuites";
import HeaderHero from "../../components/Hero/HeaderHero";

// layout
import HomeLayout from "../../leyouts/HomeLayout";

const OurSuites = () => {
  return (
    <HomeLayout>
      <HeaderHero text="Our Suites" image={"/Images/our-suites-header.jpg"} />
      <div>
        {suitData.map((suit, index) => (
          <SubSuites key={index} {...suit} />
        ))}
      </div>
    </HomeLayout>
  );
};

export default OurSuites;
