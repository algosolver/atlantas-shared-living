import { allSuitsDetails } from "../../Assets/Data/Facilities";
import SubSIngleSuit from "../../components/SubPages/singleSuite/SubSIngleSuit";
import HeaderHero from "../../components/Hero/HeaderHero";

import HomeLayout from "../../leyouts/HomeLayout";
import SubSuitDetails from "../../components/SubPages/Homepage/SubSuitDetailSection";

export const getStaticPaths = () => {
  const suites = allSuitsDetails.map((suite) => ({
    params: { suite_slug: suite.slug },
  }));
  return {
    paths: suites,
    fallback: true,
  };
};

export const getStaticProps = ({ params }) => {
  const singleSuite = allSuitsDetails?.find(
    (item) => item.slug?.toLowerCase() == params.suite_slug?.toLowerCase()
  );

  return {
    props: singleSuite || {},
  };
};

const SingleSuite = (props) => {
  if (!props?.slug) {
    return (
      <div
        style={{
          color: "000",
          background: "#fff",
          fontFamily:
            "-apple-system, BlinkMacSystemFont, Roboto, Segoe UI, Fira Sans, Avenir, Helvetica Neue, Lucida Grande, sans-serif",
          height: "100vh",
          textAlign: "center",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div>
          <h1
            className={
              "inline-block border-r border-black m-0 mr-7 pt-8 pr-16 pb-8 pl-0 text-lg font-medium"
            }
          >
            404
          </h1>
          <div className={"inline-block text-left"}>
            <h2>This page could not be found.</h2>
          </div>
        </div>
      </div>
    );
  }

  return (
    <HomeLayout>
      <HeaderHero text={props.title} image={'/Images/carouselImages/SinglePage/home-gallery-01.jpg'} />
      <div>
        <SubSIngleSuit singleSuitData={props} />
      </div>
      <div className={"mb-10 md:mb-16 px-5 md:px-20"}>
        <SubSuitDetails />
      </div>
    </HomeLayout>
  );
};

export default SingleSuite;



