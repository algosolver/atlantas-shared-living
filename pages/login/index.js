import React, { useEffect, useState } from "react";
import HomeLayout from "../../leyouts/HomeLayout";
import LoginPage from '../../components/SubPages/LoginPage';
import HeaderHero from "../../components/Hero/HeaderHero";
import { useRouter } from "next/router";

index.getInitialProps = async ({ query }) => {
    return {query}
}

function index({query}) {

  return (
    <div>
      <HomeLayout>
        <HeaderHero
          text="Log in"
          image="/Images/heroBG.jpg"
        />
        <LoginPage query={query} />
      </HomeLayout>
    </div>
  );
}

export default index;
