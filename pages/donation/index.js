import HeaderHero from '../../components/Hero/HeaderHero'
import SubDonationSection from "../../components/SubPages/Donation/SubDonationSection";
import HomeLayout from "../../leyouts/HomeLayout";

const Donation = () => {
  return (
    <HomeLayout>
        <HeaderHero image={'/Images/heroBG.jpg'}  text={'Donations'} subText={"Thank you for your donations"} height={450} />
        <SubDonationSection />
    </HomeLayout>
  );
};

export default Donation;
