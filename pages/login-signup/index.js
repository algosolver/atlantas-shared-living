import React from "react";
import HomeLayout from "../../leyouts/HomeLayout";
import LogInSignUp from "../../components/SubPages/LogInSignUp"
import HeaderHero from "../../components/Hero/HeaderHero";

index.getInitialProps = async ({ query }) => {
  return {query}
}

function index({query}) {
  return (
    <div>
      <HomeLayout>
        <HeaderHero
          text="Sign Up / Log In"
          image="/Images/heroBG.jpg"
        />
        <LogInSignUp query={query}  />
      </HomeLayout>
    </div>
  );
}

export default index;
