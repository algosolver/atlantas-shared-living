import React from 'react';
import SubActivitiesSection from '../../components/SubPages/activities/SubActivitiesSection';
import SubCallNow from '../../components/SubPages/activities/SubCallNow';
import SubHeader from '../../components/SubPages/activities/SubHeader';

import HomeLayout from "../../leyouts/HomeLayout";

const header = {
    title: 'UNIQUE ACTIVITIES',
    description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
    ut aliquip ex ea commodo consequat.`
}

const Activities = () => {
    return (
        <HomeLayout>
            <div>
                <SubHeader data={header} />
                <SubActivitiesSection/>
                <SubCallNow/>
            </div>
        </HomeLayout>
    )
}

export default Activities;
