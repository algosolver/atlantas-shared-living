import React from "react";
import Modal from "react-modal";
import { AiOutlineWarning } from "react-icons/ai";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const index = () => {
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }
  function closeModal() {
    setIsOpen(false);
  }
  return (
    <div className="bg-primary">
      <button onClick={openModal}>Open Modal</button>
      <Modal
        isOpen={modalIsOpen}
        contentLabel="Example Modal"
        className="Modal bg-gray-500 text-white text-lg text-center rounded-lg px-4 py-4"
      >
        <div className="flex justify-center text-red-400 mb-4">
          <AiOutlineWarning className="text-2xl mr-1" />
          <p className="text-xl">Warning</p>
        </div>

        <p>
          Your login Credential expired. You will be logged out automatically.
          Please login again to continue your process.
        </p>
        <button
          className="bg-green-200 px-6 text-gray-500 rounded mt-4"
          onClick={closeModal}
        >
          Ok
        </button>
      </Modal>
    </div>
  );
};

export default index;
