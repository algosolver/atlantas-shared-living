import React,{useState,useEffect} from "react";
import AxiosConfig from "../AxiosConfig/AxiosConfig"
import { useRouter } from "next/router";
import WarningModal from "../components/WarningModal";
import SubBookNow from "../components/SubPages/Homepage/SubBookNow";
import SubContactAgent from "../components/SubPages/Homepage/SubContactAgent";
import SubHeroSection from "../components/SubPages/Homepage/SubHeroSection";
import SubLegendSection from "../components/SubPages/Homepage/SubLegendSection";
import SubLuxury from "../components/SubPages/Homepage/SubLuxury";
import SubOverPricedSection from "../components/SubPages/Homepage/SubOverPricedSection";
import SubTabSection from "../components/SubPages/Homepage/SubTabSection";
import SubYourNeedSection from "../components/SubPages/Homepage/SubYourNeedSection";
import SubAreYouSection from "../components/SubPages/Homepage/SubAreYouSection";
import SubSuitDetailSection from "../components/SubPages/Homepage/SubSuitDetailSection";
import SubViewSuitesSection from "../components/SubPages/Homepage/SubViewSuitesSection";

// import layout
import HomeLayout from "../leyouts/HomeLayout";
import { FaMapMarkerAlt, FaPhone } from "react-icons/fa";

const HomePage = () => {

  const router = useRouter();
  const [modalOpen,setModalOpen] = useState(false);

  useEffect(async ()=>{
    const token = JSON.parse(localStorage.getItem("atlanta_token"));
      if(token){
        try {
          const verified_response = await AxiosConfig.get(
            "api/v1/auth/verify-token",
            {
              headers: {
                Authorization:token
              },
            }
          );

          console.log(verified_response);
        }

        catch(error){
          if(error.response.status === 401){
            

            setModalOpen(true);
          }
        }
      }
  },[]);

  return (
    <HomeLayout>
      <div>
        <section
          className={"lg:hidden absolute block text-center top-24 z-10 w-full"}
        >
          <div className={"inline"}>
            <div className="w-full justify-center flex">
              <div className={"flex lg:py-3 py-2"}>
                <div className="inline flex items-end text-yellow-primary">
                  <FaPhone size={30} />
                </div>
                <div className="pl-2 inline">
                  <p className={"text-xs uppercase text-white tracking-wide"}>
                    Reservation Number
                  </p>
                  <p
                    className={
                      "text-xl font-bold uppercase text-primary tracking-wide"
                    }
                  >
                    <a href="tel:+1-888-288-7584">1 -888-AT-ASLTH</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <br />
          <div className={"inline"}>
            <div className="w-full justify-center flex">
              <div className={"flex py-3"}>
                <div className="inline flex items-center text-yellow-primary">
                  <a
                    href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x88f5073df58fe123:0x2a81645bd4a01a1e?source=g.page.share"
                    target={"_blank"}
                  >
                    <FaMapMarkerAlt size={30} />
                  </a>
                </div>
                <div className="pl-2 inline">
                  <p className={"text-xs uppercase text-white tracking-wide"}>
                    Location
                  </p>
                  <p
                    className={
                      "text-xl font-bold uppercase text-primary tracking-wide"
                    }
                  >
                    <a
                      href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x88f5073df58fe123:0x2a81645bd4a01a1e?source=g.page.share"
                      target={"_blank"}
                    >
                      georgia
                    </a>
                  </p>
                  <span className={"text-xs text-white"}>
                    Office visits by appointment only!
                  </span>
                </div>
              </div>
            </div>
          </div>
        </section>
        <SubHeroSection />
        <SubViewSuitesSection />
        <SubLegendSection />
        <SubAreYouSection />
        <SubYourNeedSection />
        <SubTabSection />
        <SubBookNow />
        <SubLuxury />
        <SubSuitDetailSection />
        <SubOverPricedSection />
        <SubContactAgent />
        <WarningModal modalOpen={modalOpen} setModalOpen={setModalOpen}/>
      </div>
    </HomeLayout>
  );
};

export default HomePage;
