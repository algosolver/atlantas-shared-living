import React from 'react';
import AboutContact from '../../components/SubPages/AboutUs/AboutContact';
import Statements from '../../components/SubPages/AboutUs/Statements';
import HeaderHero from "../../components/Hero/HeaderHero";

import HomeLayout from "../../leyouts/HomeLayout";

const data = {
  heroImage: "/Images/AboutUs/heroBG.jpg",
  heroDescription:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
  aboutLogo: "/Images/AboutUs/aboutBG.png",
  statements: [
    {
      title: "OUR MISSION",
      description:
        "ASLTHinc promises to provide the perfect living solutions customized for those in transition. We promise to provide an upscale, safer, flexible, non-traditional way of living.",
    },
    {
      title: "OUR VISION",
      description:
        "No place in the Metro Atlanta area can you find a more competitive advantage. Our vision like no other, provides: sustainable, creative, upscale, flexible & affordable shared transitional living. ",
    },
  ],
  designImage: "/Images/v-shape-image.png",
};

const AboutUs = () => {
    return (
        <HomeLayout>
            <div>
                <HeaderHero
                    text={'ABOUT US'}
                    image={'/Images/about-us-header.jpg'}
                />
                <Statements data={data.statements} />
                <AboutContact designImage={data.designImage} />
            </div>
        </HomeLayout>
    )
}

export default AboutUs
