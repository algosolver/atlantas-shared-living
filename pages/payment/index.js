import React, {useEffect,useState} from "react";
import HomeLayout from "../../leyouts/HomeLayout";
import HeaderHero from "../../components/Hero/HeaderHero";
import RequestToBook from "../../components/SubPages/Payment/RequestToBook";
import Summary from "../../components/SubPages/Payment/Summary";
import AxiosConfig from "../../AxiosConfig/AxiosConfig";

const Payment = () => {

  const [amount, setAmount] = useState();

  useEffect(() => {
    getAllInfo();
  }, []);

  const getAllInfo = async () => {
    const booking = JSON.parse(localStorage.getItem("booking-info"));
    console.log(booking);
    try {
      const response = await AxiosConfig.get("/api/v1/location");
      let price = 0;
      if (response) {
        const info = response.data.results;
        const location = info.find(
          (item) => item.id === booking.locationId.value
        );
        const property = location?.properties.find(
          (item) => item.id === booking.propertyId.value
        );
        const suite = property?.suites.find(
          (item) => item.id === booking.suiteId.value
        );

        booking.bedIds.map((bed) => {
          const bedInfo = suite.beds.find((item) => {
            return item.id === bed;
          });
          price += bedInfo.price;
        });

        setAmount(price);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <HomeLayout>
      <HeaderHero text="Payment" image={"/Images/our-suites-header.jpg"} />
      <div className="mx-auto px-5 md:px-56 py-10 md:py-15  text-gray-800 flex flex-col-reverse md:flex-row flex-wrap justify-around">
        <div className="w-full lg:w-1/2 flex-0 p-5 bg-white">
          <RequestToBook amount={amount}/>
        </div>
        <div className="w-full lg:w-1/3 flex-0 ">
          <Summary amount={amount}/>
        </div>
      </div>
    </HomeLayout>
  );
};

export default Payment;
