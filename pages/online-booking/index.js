import React from "react";
import SubBookingSuits from "../../components/SubPages/OnlineBooking";
import { suite_options } from "../../Assets/Data/FormData";


import HomeLayout from "../../leyouts/HomeLayout";

const Onlinebooking = () => {
  return (
    <HomeLayout>
      <SubBookingSuits suite_options={suite_options} />
    </HomeLayout>
  );
};

export default Onlinebooking;
