import { useEffect } from "react";
import AOS from 'aos';

import '../styles/global.css'
import 'aos/dist/aos.css'
// redux-toolkit

import {Provider} from "react-redux"
import {store} from "../redux/store"



function MyApp({ Component, pageProps }) {

  useEffect(() => {
    AOS.init();
  }, [])

  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

export default MyApp
