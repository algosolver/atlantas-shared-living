import React from 'react';
import SubContactDetails from '../../components/SubPages/ContactUs/SubContactDetails';
import SubContactForm from '../../components/SubPages/ContactUs/SubContactForm';
import SubContactNow from '../../components/SubPages/ContactUs/SubContactNow';
import SubHero from '../../components/SubPages/ContactUs/SubHero';

// layout
import HomeLayout from "../../leyouts/HomeLayout";

const contactDetails = {
    reservationNum: '1-404-92-19598',
    location: 'ONE WEST COURT SQUARE SUITE # 750 DECATUR, GEORGIA 30030.',
    email: 'aslthinc@gmail.com',
}
const contactForm = {
    title: 'MAKE A RESERVATION',
    description: `Anyone who appreciates an affordable, flexible shared living arrangement please feel free to make a call for information or reservation. We are just a call away from you.`
}

const ContactUs = () => {

    return (
        <HomeLayout>
            <div>
                <SubHero/>
                <SubContactDetails contactDetails={contactDetails}  />
                <SubContactForm data={contactForm} />
                <SubContactNow/>
            </div>
        </HomeLayout>
    )
}

export default ContactUs; 
