import React, { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import MenuItems from "./menuItems";
import ModalWrapper from "../../../components/Modal/ModalWrapper";
import { useDispatch } from "react-redux";
import { toggleModalState } from "../../../redux/slices/modalSlice";
import { useSelector } from "react-redux";
import {
  FaPhone,
  FaMapMarkerAlt,
  FaLongArrowAltRight,
  FaBars,
  FaTimes,
} from "react-icons/fa";

function Header() {
  const isSingedIn = useSelector((state)=>state.auth.emailSubmitted);
  const router = useRouter();

  const menu = useRef(null);

  const [pos, setPos] = useState(0);
  const [collapseMenu, setCollapseMenu] = useState(false);
  const [token, setToken] = useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    setPos(window.pageYOffset);
    window.onscroll = (e) => setPos(window.pageYOffset);
  }, []);

  useEffect(()=>{
    let temp = localStorage.getItem('atlanta_token');
    setToken(temp);
  }, [])

  useEffect(() => {
    if (collapseMenu) {
      menu.current.classList.remove("hidden");
      menu.current.classList.add("fixed");
    } else {
      menu.current.classList.remove("fixed");
      menu.current.classList.add("hidden");
    }
  }, [collapseMenu]);

  return (
    <>
      <header
        className={
          pos < 100
            ? "sticky lg:absolute border-gray-500 bg-white lg:bg-transparent lg:border-b-0 -top-0 z-30 transition duration-300 w-full"
            : "w-full sticky border-gray-500 -top-0 z-30 transition duration-300 bg-white"
        }
      >
        <div>
          <div className={"grid grid-cols-4"}>
            <div className="col-span-2 lg:col-span-1">
              <div
                className={
                  pos < 100
                    ? "flex justify-center items-end lg:pt-7"
                    : "flex justify-center items-end"
                }
              >
                <img
                  onClick={() => router.push("/")}
                  src={"/Images/Logo.png"}
                  className={
                    pos < 100
                      ? "h-16 px-2 lg:h-24 py-2 cursor-pointer"
                      : "h-16 px-2 lg:h-20 py-2 cursor-pointer"
                  }
                />
              </div>
            </div>

            <div className="col-span-2 lg:col-span-3">
              <div className={"w-full"}>
                <div className={"grid grid-cols-12"}>
                  <div className={pos > 100 ? "col-span-3" : "col-span-4"}>
                    <div className="w-full justify-center hidden lg:flex">
                      <div
                        className={
                          pos < 100 ? "flex lg:py-5 py-2" : "flex lg:py-3 py-2"
                        }
                      >
                        <div className="inline flex items-end text-yellow-primary">
                          <FaPhone size={34} />
                        </div>
                        <div className="pl-2 inline">
                          <p
                            className={
                              pos < 100
                                ? "tracking-wide text-xs font-light lg:font-bold uppercase text-white"
                                : "text-xs font-light lg:font-bold uppercase text-gray-500 tracking-wide"
                            }
                          >
                            Reservation Number
                          </p>
                          <p
                            className={
                              "text-md lg:text-2xl font-medium lg:font-extrabold uppercase text-primary tracking-wide"
                            }
                          >
                            <a href="tel:+1-888-288-7584">1 -888-AT-ASLTH</a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={pos > 100 ? "col-span-3" : "col-span-4"}>
                    <div className="w-full justify-center hidden lg:flex">
                      <div className={pos < 100 ? "flex py-5" : "flex py-3"}>
                        <div className="inline flex items-center text-yellow-primary">
                          <a
                            href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x88f5073df58fe123:0x2a81645bd4a01a1e?source=g.page.share"
                            target={"_blank"}
                          >
                            <FaMapMarkerAlt size={34} />
                          </a>
                        </div>
                        <div className="pl-2 inline">
                          <p
                            className={
                              pos < 100
                                ? "tracking-wide text-xs font-bold uppercase text-white"
                                : "text-xs font-bold uppercase text-gray-500 tracking-wide"
                            }
                          >
                            <a
                              href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x88f5073df58fe123:0x2a81645bd4a01a1e?source=g.page.share"
                              target={"_blank"}
                            >
                              Location
                            </a>
                          </p>
                          <p
                            className={
                              "text-2xl font-extrabold uppercase text-primary tracking-wide"
                            }
                          >
                            <a
                              href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x88f5073df58fe123:0x2a81645bd4a01a1e?source=g.page.share"
                              target={"_blank"}
                            >
                              georgia
                            </a>
                          </p>
                          <span className={
                            pos < 100
                                ? "text-xs text-white"
                                : "text-xs text-yellow-primary font-light"
                          }>Office visits by appointment only!</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={pos > 100 ? "col-span-6" : "col-span-4"}>
                    <div
                      className={
                        pos > 100
                          ? "flex justify-end lg:justify-around items-center py-3"
                          : "flex lg:justify-around items-center justify-end py-5"
                      }
                    >
                      <button
                        onClick={()=>{
                          token ? router.push("/online-booking") : router.push({pathname: '/login-signup',query: { next: 'online-booking' },}); 
                        }}
                        className={
                          "hidden lg:flex focus:outline-none text-gray-500 bg-yellow-primary transition duration-300 hover:bg-gray-500 hover:text-white uppercase text-xl font-bold py-5 tracking-wider px-7"
                        }
                      >
                        Book Now
                        <span className={"pl-4 flex items-center h-full"}>
                          <FaLongArrowAltRight />
                        </span>
                      </button>
                      {collapseMenu == false && (
                        <button
                          onClick={() => setCollapseMenu(true)}
                          className={
                            pos > 100
                              ? "focus:outline-none text-primary mr-5 lg:mr-3"
                              : "lg:hidden mr-5 text-primary lg:mr-3 focus:outline-none"
                          }
                        >
                          <FaBars size={34} />
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              {pos < 100 && (
                <div
                  className={
                    "hidden lg:block w-11/12 mx-auto border-t border-gray-200"
                  }
                >
                  <div className={"desktop-init-menu"}>
                    <MenuItems />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
        <div
          ref={menu}
          className={
            "transition duration-500 -top-0 -left-0 h-screen w-screen bg-green-900 bg-opacity-90 flex justify-center items-center"
          }
          style={{ zIndex: "999" }}
          onClick={() => setCollapseMenu(false)}
        >
          <button
            className={"focus:outline-none fixed right-5 text-white top-5"}
            onClick={() => setCollapseMenu(false)}
          >
            <FaTimes size={32} />
          </button>
          <div className={"collapse-menu"}>
            <MenuItems />
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
