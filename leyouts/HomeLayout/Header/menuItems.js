import Link from 'next/link'
import { useRouter } from "next/router";
import { useEffect, useState } from 'react';

const MenuItems = () => {
    const router = useRouter()

    const [token, setToken] = useState('');

    useEffect(()=>{
        setToken(localStorage.getItem('atlanta_token'))
        console.log(token);
    }, [])

    const handleSingOut = () => {
      localStorage.removeItem("atlanta_token");
      if (router.pathname === "/") {
        router.reload();
      } else {
        router.push("/");
      }
    };

    return (
        <ul>
            <li>
                <Link href={'/'}>
                    <a className={router.pathname === '/' ? 'text-yellow-primary': ''}>Home</a>
                </Link>
            </li>
            <li>
                <Link href={'/our-suites'}>
                    <a className={router.pathname === '/our-suites' ? 'text-yellow-primary': ''}>Our Suites</a>
                </Link>
            </li>
            <li><Link href={'#'}> Reservation</Link></li>
            <li><Link href={'/donation'}><a className={router.pathname === '/donation' ? 'text-yellow-primary': ''}> Donations</a></Link></li>
            <li><Link href={'#'}>Shop</Link></li>
            <li><Link href={'#'}>Blog</Link></li>
            <li>
                <Link href={'/about-us'}>
                    <a className={router.pathname === '/about-us' ? 'text-yellow-primary': ''}> About Us</a>
                </Link>
            </li>
            {/* <li><Link href={'/activities'}>Activities</Link></li>
            <li><Link href={'/gallery'}>Gallery</Link></li> */}
            <li>
                <Link href={'/contact-us'}>
                    <a className={router.pathname === '/contact-us' ? 'text-yellow-primary': ''}>Contact us</a>
                </Link>
            </li>
            {
                token ? (
                    <li>
                        <p className={'uppercase cursor-pointer'} onClick={handleSingOut}>Signout</p>
                    </li>
                    
                ) : (
                    <li>
                        <Link href={{
                            pathname: '/login-signup',
                            query: { next: '' },
                        }}>
                            <a className={router.pathname === '/login-signup' ? 'text-yellow-primary': ''}>Login/SignUp</a>
                        </Link>
                    </li>
                )
            }
            
        </ul>
    )
}

export default MenuItems
