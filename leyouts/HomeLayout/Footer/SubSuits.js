import React from "react";
import Link from "next/link";

const SubSuits = () => {
  return (
    <div className={"col-span-10 md:col-span-2"}>
      <h6 className="text-base  font-semibold text-black uppercase mb-8">
        SUITES
      </h6>
      <div className={"mt-6"}>
        <div className={"mb-1"}>
          <Link
            href="/our-suites/presidential"
            className={
              "text-xs md:text-sm text-center md:text-left  font-normal"
            }
          >
            Presidential Suites
          </Link>
        </div>

        <div className={"mb-1"}>
          <Link
            href="/our-suites/governor"
            className={
              "text-xs md:text-sm text-center md:text-left  font-normal"
            }
          >
            Governor Suite
          </Link>
        </div>

        <div className={"mb-1"}>
          <Link
            href="/our-suites/mayor"
            className={
              "text-xs md:text-sm text-center md:text-left  font-normal"
            }
          >
            Mayor Suite
          </Link>
        </div>

        <div className={"mb-1"}>
          <Link
            href="/our-suites/executive"
            className={
              "text-xs md:text-sm text-center md:text-left  font-normal"
            }
          >
            Executive Suite
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SubSuits;
