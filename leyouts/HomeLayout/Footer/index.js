import React from "react";
import SubCopyRight from "./SubCopyRight";
import SubImage from "./SubImage";
import SubSiteMaps from "./SubSiteMaps";
import SubSocial from "./SubSocial";
import SubSuits from "./SubSuits";
import SubWarning from "./SubWarning";

const Footer = () => {
  return (
    <div>
      <div className="grid w-9/12 md:w-11/12 sm:w-full mx-auto  grid-cols-12  gap-7 md:px-10 sm:px-5 py-10">
        <SubImage />
        <SubWarning />
        <SubSocial />
        <SubSiteMaps />
        <SubSuits />
      </div>
      <SubCopyRight />
    </div>
  );
};

export default Footer;
