import React from "react";

const SubCopyRight = () => {
  return (
    <div className="border-t border-solid border-gray-300 mt-4 py-4">
      <div className="container px-4 mx-auto my-7">
        <p className={"text-center text-xs md:text-sm"}>
          @{new Date().getFullYear()} ATLANTA’S SHARED LIVING. ALL RIGHTS
          RESERVED | CREATED BY Algo Solver LLC.
        </p>
      </div>
    </div>
  );
};

export default SubCopyRight;
