import React from "react";

const SubWarning = () => {
  return (
    <div className={"col-span-10 md:col-span-3"}>
      <h6 className="text-base w-2/3 font-semibold text-red-600 uppercase mb-8">
        Warning
      </h6>
      <div className={"mr-3"}>
        <p className="text-gray-600 font-normal ">
          Under Georgia law, there is no liability for an injury or death of an
          individual entering these premises if such injury or death results
          from the inherent risks of contracting COVID-19. You are assuming this
          risk by entering these premises.
        </p>
        <p className="text-gray-600 font-normal mt-2">O.C.G.A. 51-16-3</p>
      </div>
    </div>
  );
};

export default SubWarning;
