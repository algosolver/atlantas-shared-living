import React from "react";
import Link from "next/link";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaDribbble,
  FaYoutube,
} from "react-icons/fa";

const SubSocial = () => {
  return (
    <div className={"col-span-10 md:col-span-3"}>
      <h6 className="text-base w-2/3 font-semibold text-black uppercase mb-8">
        WE ARE WORKING TO GIVE YOU THE MOST PERFECT SERVICE.
      </h6>
      <div className={"mr-3"}>
        <p className="text-gray-600 font-normal ">
          Prices and availability are subject to change at anytime. To protect
          guests privacy photos are not actual images of our suites and are used
          for marketing, sales and promotional purposes only. Because we do not
          run background checks valid ID and social security card is required in
          order to book our suites. Must be 18+ to book a suite. Additional
          terms and conditions may apply.
        </p>
      </div>
      <div className={"mt-10"}>
        <div className="flex justify-items-start text-lg mb-4" role="group">
          <Link href="/">
            <a className="bg-black border border-black text-white hover:text-black hover:bg-white px-4 py-2 mx-0 outline-none focus:shadow-outline">
              <FaFacebookF />
            </a>
          </Link>
          <Link href="/">
            <a className="bg-black border border-black text-white hover:text-black hover:bg-white   px-4 py-2 mx-0 outline-none focus:shadow-outline">
              <FaTwitter />
            </a>
          </Link>
          <Link href="/">
            <a className="bg-black border border-black text-white hover:text-black hover:bg-white   px-4 py-2 mx-0 outline-none focus:shadow-outline">
              <FaInstagram />
            </a>
          </Link>
          <Link href="/">
            <a className="bg-black border border-black text-white hover:text-black hover:bg-white   px-4 py-2 mx-0 outline-none focus:shadow-outline">
              <FaDribbble />
            </a>
          </Link>
          <Link href="/">
            <a className="bg-black border border-black text-white hover:text-black hover:bg-white px-4 py-2 mx-0 outline-none focus:shadow-outline">
              <FaYoutube />
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SubSocial;
