import React from "react";
import Link from "next/link";

const SubSiteMaps = () => {
  return (
    <div className={"col-span-10 md:col-span-2"}>
      <h6 className="text-base  font-semibold text-black uppercase mb-8">
        SITEMAP
      </h6>
      <div className={"mt-6"}>
        <div className={"mb-1"}>
          <Link
            href="/"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            HOME
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="/our-suites"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            OUR SUITES
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="#"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            RESERVATIONS
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="/donation"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            DONATIONS
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="#"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            SHOP
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="/"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            BLOG
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="/about-us"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            ABOUT US
          </Link>
        </div>
        <div className={"mb-1"}>
          <Link
            href="/contact-us"
            className={
              "text-xs md:text-sm text-center md:text-left uppercase font-normal"
            }
          >
            CONTACT US
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SubSiteMaps;
