import mongoose from "mongoose";

const connectDB = handler => async (req, res) => {
    if(mongoose.connections[0].readyState){
        // use current db connection
        return handler(req, res)
    }
    // use new db connection
    await mongoose.connect('mongodb+srv://dev:Ab00ZLQHv5GbjeMP@cluster0.aesk8.mongodb.net/veteran-booking?retryWrites=true&w=majority', {
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useNewUrlParser: true
    })
    return handler(req, res);
}

export default connectDB;
