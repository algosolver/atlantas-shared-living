import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const calender = new Schema({
    suiteId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Suite'
    }, 
    date: {
        type: Schema.Types.Date,
        required: false
    }, 
    deletdAt: {
        type: Schema.Types.Date,
        default: null
    }
},{
    timestamps : true
});


const Calender = mongoose.models?.Calender || mongoose.model('Calender', calender);


export default Calender;