import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import Suite from './suite'

const reservation = new Schema({
    reservationNo: {
        type: String,
        required: true,
        unique: true
    },
    customerName: {
        type: String,
        required: true
    },
    customerEmail:{
        type: Schema.Types.String,
        default: null,
        required: false
    },
    customerPhone: {
        type: Schema.Types.String,
        required: true
    },
    suite: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Suite'
    },
    suiteTitle: {
        type: String,
        required: true
    },
    checkin: {
        type: Schema.Types.Date,
        required: true
    },
    checkout: {
        type: Schema.Types.Date,
        required: true
    },
    adult: {
        type: Schema.Types.Number,
        required: true
    }, 
    child: {
        type: Schema.Types.Number,
        required: true,
        default: 0
    },
    fare: {
        type: Number,
        required: true
    },
    subTotal: {
        type: Number,
        required: true
    }, 
    discount: {
        type: Number,
        required: false,
        default: null
    },
    total: {
        type: Number,
        required: true
    },
    coupon: {
        type: String,
        required: false,
        default: null
    },
    status: {
        type: String,
        enum: ['pending','approved','cancel'],
        required: true,
        default: 'pending'
    },
    paymentStatus: {
        type: String,
        enum: ['pending','confirmed','cancel'],
        required: true,
        default: 'pending'
    },
    deletedAt: {
        type: Date,
        default: null
    }
},{
    timestamps : true
});


const Reservation = mongoose.models?.Reservation || mongoose.model('Reservation', reservation);


export default Reservation;
