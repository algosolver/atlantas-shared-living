import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const suite = new Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
        unique: true,
        required: true
    },
    thumbnail:{
        type: Schema.Types.String,
        required: false
    },
    description: {
        type: Schema.Types.String,
        required: false
    },
    adult: {
        type: Schema.Types.Number,
        required: true
    }, 
    child: {
        type: Schema.Types.Number,
        required: true
    },
    fare: {
        type: Schema.Types.Number,
        required: true
    },
    visible: {
        type: Schema.Types.Boolean,
        required: true,
        default: true
    },
    // for now, it's string, it will be list of array
    amenity: {
        type : Schema.Types.String,
        required: false
    },
    deletedAt: {
        type: Date,
        default: null
    }
},{
    timestamps : true
});


const Suite = mongoose.models?.Suite || mongoose.model('Suite', suite);


export default Suite;
