import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./slices/counter";
import modalReducers from "./slices/modalSlice";
import authReducres from "./slices/authSlice";
import bookingReducres from "./slices/bookingSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    modal: modalReducers,
    auth: authReducres,
    booking:bookingReducres
  },
});
