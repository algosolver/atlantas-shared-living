import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
  name: "modal",
  initialState: {
    wrapperModalisOpen: undefined,
  },
  reducers: {
    toggleModalState: (state) => {
      state.wrapperModalisOpen = !state.wrapperModalisOpen
    },
  },
  extraReducers: {},
});

export const { toggleModalState } = modalSlice.actions;

export default modalSlice.reducer;

export const modalSelector = (state) => state.modal;
