import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  bookingInfo:{
      checkIn:null,
      checkOut:null,
      suite:null,
      bed:null,
      adult:null,
      child:null
  }
};

const bookingSlice = createSlice({
  name: "booking",
  initialState,
  reducers: {
    addDate: (state, action) => {
        state.bookingInfo.checkIn = action.payload[0];
        state.bookingInfo.checkOut = action.payload[1];
    },
    addSuite: (state, action) => {
        state.bookingInfo.suite = action.payload;
    },
    addBed: (state, action) => {
        state.bookingInfo.bed = action.payload;
    },
    addAdult: (state, action) => {
        state.bookingInfo.adult = action.payload;
    },
    addChild: (state, action) => {
        state.bookingInfo.child = action.payload;
    },
  },
  extraReducers: {},
});

export const { addDate,addSuite,addBed,addAdult,addChild } =
  bookingSlice.actions;

export default bookingSlice.reducer;

export const bookingSelector = (state) => state.booking;
