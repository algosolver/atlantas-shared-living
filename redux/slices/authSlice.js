import { createSlice } from "@reduxjs/toolkit";


const initialState = {
  emailSubmitted: false,
  userInfo: {},
  signedIn: false,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    emailSubmission: (state) => {
      state.emailSubmitted = true;
    },

    

    signUp: (state, action) => {
      state.userInfo = action.payload;
    },

    signIn: (state, action) =>{
      state.userInfo = action.payload;
      state.signedIn = true;
    },

    addImages: (state, action) => {
      const { selectDl, selectPi } = action.payload;
      return {
        ...state,
        userInfo: {
          ...state.userInfo,
          drivingLicense: selectDl,
          profileImage: selectPi,
        },
      };
    },

    decline: (state) => {
      state.userInfo = initialState.userInfo;
    },
  },
  extraReducers: {},
});

export const { emailSubmission, signUp, signIn, decline, addImages, addProfileImage } =
  authSlice.actions;

export default authSlice.reducer;
