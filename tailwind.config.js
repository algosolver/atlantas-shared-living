module.exports = {
  mode: "jit",
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./leyouts/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        primary: ["sans-serif", "Montserrat"],
        secondary: ["Roboto", "sans-serif"],
        suit: ["Eczar", "serif"],
      },
      fontSize: {
        "head-1": "85px",
        "head-2": "77px",
        "head-3": "65px",
        "head-4": "55px",
        "head-5": "48px",
        "head-6": "40px",
        "head-7": "36px",
        "head-8": "35px",
        "sub-head-1": "28px",
        "sub-head-2": "24px",
        "sub-head-3": "21px",
        "sub-head-4": "20px",
        "sub-head-5": "19px",
        "sub-head-6": "17px",
        paragraph: "16px",
        "paragraph-sm": "14px",
      },
      colors: {
        primary: "#7ECDCB",
        secondary: "#261F1F",
        grey: "#5a5a5a",
        yellowButton: "#edec68",
        fbIconColor:"#4267B2",
        "yellow-primary": "#edec68",
        stripeButton: "#32a6e4",
        warningRed:"#ff0f0f"
      },
      width: {
        55: "55%",
        40: "30%",
        70: "70%",
      },
      maxWidth: {
        max30: "30%",
      },

      height: {
        // prettier-ignore
        "suit-height":"25rem",
        "detailed-suit-height": "30rem",
        "vh-70": "70vh",
        "vh-30": "30vh",
        "vh-50": "50vh",
      },
      borderWidth: {
        DEFAULT: "1px",
        0.5: "0.5",
      },
    },
  },
  variants: {
    extend: {
      margin: ["even"],
    },
  },
  plugins: [],
};
