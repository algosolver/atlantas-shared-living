import React from "react";

const SuitImage = ({ image }) => {
  return (
    <div className="flex justify-center items-center h-80  md:h-96  md:w-2/3">
      <img src={image} alt="suit" className={"w-full h-full border-gray-300 object-cover"} />
    </div>
  );
};

export default SuitImage;
