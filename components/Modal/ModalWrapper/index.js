import React, { useEffect } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import {
  modalSelector,
  toggleModalState,
} from "../../../redux/slices/modalSlice";

const ModalWrapper = ({ children }) => {
  const { wrapperModalisOpen } = useSelector(modalSelector);
  const dispatch = useDispatch();

  

  const toggleModal = () => {
    const modal = document.querySelector(".modal");
    modal.classList.toggle("opacity-0");
    modal.classList.toggle("pointer-events-none");
  };

  useEffect(() => {
    if (wrapperModalisOpen === true || wrapperModalisOpen === false) {
      toggleModal();
    }
  }, [wrapperModalisOpen]);

  return (
    <div className="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center z-50">
      <div
        className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"
        // onClick={() => dispatch(toggleModalState())}
      ></div>

      <div className="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
        <div className="modal-content py-4">
          {/* <div className="flex justify-end items-center pb-3 px-6">
            <div
              className="modal-close cursor-pointer z-50 text-right"
              onClick={() => dispatch(toggleModalState())}
            >
              <AiOutlineClose />
            </div>
          </div> */}
          {children}
        </div>
      </div>
    </div>
  );
};

export default ModalWrapper;
