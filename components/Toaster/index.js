import React from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const contextClass = {
  success: "bg-blue-600",
  error: "bg-red-600",
  info: "bg-gray-600",
  warning: "bg-orange-400",
  default: "bg-red-600",
  dark: "bg-white-600 font-gray-300",
};

const Toaster = () => {
  return (
    <ToastContainer
      position="bottom-right"
      autoClose={3000}
      toastClassName={({ type }) =>
        contextClass[type || "error"] +
        " relative flex p-1 min-h-10 rounded-md justify-between overflow-hidden cursor-pointer"
      }
      bodyClassName={() => "text-sm font-white font-med block p-3"}
    />
  );
};

export default Toaster;
