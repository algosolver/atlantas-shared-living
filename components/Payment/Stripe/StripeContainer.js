import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import PaymentForm from './PaymentForm';
import Toaster from "../../Toaster";


const PUBLIC_KEY ="pk_test_51JDrfGL6dPTcqE42bUjVOMBz9IlrztzO3WQSLYuIsVaQik9uBgC5l3ubS3NngycpFfOZDAXHHBDzL9CghHFjkkzd00LKieQOlz";


const stripePromise = loadStripe(PUBLIC_KEY);

const StripeContainer = ({amount}) => {
    return (
        <div>
            <Elements stripe={stripePromise}>
                <PaymentForm amount={amount}/>
            </Elements>
            <Toaster />
        </div>
        
    );
}

export default StripeContainer;