import { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { toast } from "react-toastify";
import AxiosConfig from "../../../AxiosConfig/AxiosConfig";
import { useRouter } from "next/router";
import PaymentSuccessModal from "../../PaymentSuccessModal";

const card_options = {
  iconStyle: "solid",
  style: {
    base: {
      iconColor: "#9f9e97",
      color: "#000",
      fontWeight: "500",
      fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
      fontSize: "22px",
      fontSmoothing: "antialiased",
      ":-webkit-autofill": {
        color: "#fce883",
      },
      "::placeholder": {
        color: "#9f9e97",
      },
    },
    invalid: {
      iconColor: "#ff4c4c",
      color: "#ff4c4c",
    },
  },
};

const PaymentForm = ({ amount }) => {
  const stripe = useStripe();
  const elements = useElements();
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleSubmit = async (event) => {
    event.preventDefault();
    setLoading(true);
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
    });
    if (!error) {
      const bookingInfo = JSON.parse(localStorage.getItem("booking-info"));
      const token = JSON.parse(localStorage.getItem("atlanta_token"));
      if (bookingInfo) {
        const { locationId, propertyId, suiteId, ...rest } = bookingInfo;

        const allData = {
          ...rest,
          amount,
          totalPaid: amount + 100,
          token: paymentMethod.id,
          currency: "usd",
        };

        const response = await AxiosConfig.post("api/v1/booking", allData, {
          headers: { Authorization: token },
        });

        if (response.data.statusCode === 201) {
          setModalIsOpen(true);
          localStorage.removeItem("booking-info");
          setLoading(false);
        } else {
          toast("Something Went Wrong!! Please try again later");
          router.push("/");
        }
      }
    } else {
      // console.log(error.message);
      toast("Something Went Wrong!! Please try again later");
      router.push("/");
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className={"px-2 py-3 border-gray-400 border rounded"}>
          <CardElement options={card_options} />
        </div>
        <div className={"flex justify-center mt-5"}>
          <button
            className={
              "w-full h-14 font-bold bg-stripeButton text-xl rounded text-white"
            }
            type="submit"
            disabled={loading}
          >
            {loading ? "loading..." : "Proceed Payment"}
          </button>
        </div>
      </form>
      <PaymentSuccessModal
        modalOpen={modalIsOpen}
        setModalOpen={setModalIsOpen}
      />
    </div>
  );
};

export default PaymentForm;
