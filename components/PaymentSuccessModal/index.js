import React from "react";
import Modal from "react-modal";
import { GiConfirmed } from "react-icons/gi";
import { useRouter } from "next/router";

const PaymentSuccessModal = ({ modalOpen, setModalOpen }) => {
  const router = useRouter();

  function closeModal() {
    if (router.pathname === "/") {
      router.reload();
    } else {
      router.push("/");
    }
    setModalOpen(false);
  }
  return (
    <div className="bg-primary">
      <Modal
        isOpen={modalOpen}
        contentLabel="Example Modal"
        className="Modal bg-gray-500 text-white text-lg text-center rounded-lg px-4 py-4"
        ariaHideApp={false}
      >
        <div className="flex justify-center text-green-400 mb-4">
          <GiConfirmed className="text-2xl mr-1" />
          <p className="text-xl">Success</p>
        </div>

        <p>
          Your Payment is Successful. Please check your mail for confirmation.
        </p>
        <button
          className="bg-green-200 px-6 text-gray-500 rounded mt-4"
          onClick={() => closeModal()}
        >
          Ok
        </button>
      </Modal>
    </div>
  );
};

export default PaymentSuccessModal;

  

