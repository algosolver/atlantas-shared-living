import React from "react";
import Modal from "react-modal";
import { AiOutlineWarning } from "react-icons/ai";
import {useRouter} from "next/router"

const WarningModal = ({modalOpen,setModalOpen}) => {

    const router = useRouter();
  
  function closeModal() {
     localStorage.removeItem("atlanta_token");
     if (router.pathname === "/") {
       router.reload();
     } else {
       router.push("/");
     }
    setModalOpen(false);
  }
  return (
    <div className="bg-primary">
      <Modal
        isOpen={modalOpen}
        contentLabel="Example Modal"
        className="Modal bg-gray-500 text-white text-lg text-center rounded-lg px-4 py-4"
        ariaHideApp={false}
      >
        <div className="flex justify-center text-red-400 mb-4">
          <AiOutlineWarning className="text-2xl mr-1" />
          <p className="text-xl">Warning</p>
        </div>

        <p>
          Your login Credential expired. You will be logged out automatically.
          Please login again to continue your process.
        </p>
        <button
          className="bg-green-200 px-6 text-gray-500 rounded mt-4"
          onClick={() => closeModal()}
        >
          Ok
        </button>
      </Modal>
    </div>
  );
};

export default WarningModal;

  

