const formateDate = (date) => {
    
      const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ];

      let formatted_date =
        date.getDate() + "-" + months[date.getMonth()] + ", " + date.getFullYear();

      return formatted_date;
    
}

export default formateDate
