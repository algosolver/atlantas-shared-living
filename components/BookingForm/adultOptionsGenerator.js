function adultOptionsGenerator(suiteId) {
  let max;
  const options = [];

  switch (suiteId) {
    case 1:
      max = 2;
      break;
    case 2:
      max = 4;
      break;
    case 3:
      max = 6;
      break;
    case 4:
      max = 8;
  }

  for (let i = 0; i < max; i++) {
    options[i] = { value:i+1, label:i+1 };
  }

  return options;
}

export default adultOptionsGenerator;
