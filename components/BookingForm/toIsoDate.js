const toIsoDate = (date) => {
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  let formatted_date = `${date.getFullYear()}-${
    date.getMonth() + 1 < 10
      ? `0${date.getMonth() + 1}`
      : `${date.getMonth() + 1}`
  }-${
    date.getDate()< 10
      ? `0${date.getDate()}`
      : `${date.getDate()}`
  }T00:00:00.000Z`; ;

  return formatted_date;
};

export default toIsoDate;
