function childOptionsGenerator(suiteId) {
  let max;
  const options = [];

  switch (suiteId) {
    case 1:
      max = 2;
      break;
    case 2:
      max = 2;
      break;
    case 3:
      max = 3;
      break;
    case 4:
      max = 4;
  }

  for (let i = 0; i <= max; i++) {
    options[i] = { value: i, label: i};
  }

  return options;
}

export default childOptionsGenerator;
