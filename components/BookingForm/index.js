import { useState, useEffect } from "react";
import AxiosConfig from "../../AxiosConfig/AxiosConfig";
import Select from "react-select";
import DatePicker from "./DatePicker";
import { useRouter } from "next/router";
import adultOptionsGenerator from "./adultOptionsGenerator";
import childOptionsGenerator from "./childOptionsGenerator";
import { toast } from "react-toastify";
import Toaster from "../Toaster";

const index = () => {
  //Loading States

  const [locationLoading, setLocationLoading] = useState(true);
  const [bedLoading, setBedLoading] = useState(false);

  const router = useRouter();

  // Request button disble state

  const [buttonDisable, setButtonDisable] = useState(true);

  //initial options state for location and bed field

  const [locationOptions, setLocationOptions] = useState(null);
  const [bedOptions, setBedOptions] = useState([]);

  // api response data will be stored here

  const [apiData, setApiData] = useState(null);

  const [token, setToken] = useState("");

  //after selecting the option will be saved in these states

  const [selectedLocationId, setSelectedLocationId] = useState(null);
  const [selectedPropertyId, setSelectedPropertyId] = useState(null);
  const [selectedSuiteId, setSelectedSuiteId] = useState(null);
  const [selectedBedsId, setSelectedBedsId] = useState(null);
  const [adultCount, setAdultCount] = useState(null);
  const [childCount, setChildCount] = useState(null);
  const [dates, setDates] = useState(null);
  const [amount, setAmount] = useState(0);

  //this useEffect will fetch all data from backend when page initailly starts

  useEffect(async () => {
    try {
      const response = await AxiosConfig.get("/api/v1/location");
      if (response) {
        setApiData(response.data.results); //saving data to the local state for further use

        filterLocationOptions(response.data.results); //filtering location options from reponse data
      }
    } catch (error) {
      toast.error("Please check your network");
    }
  }, []);

  //this useEffect will set property, suite and date field to null if location id changes

  useEffect(() => {
    setSelectedPropertyId(null); //clearing property field  if location changes after selecting property
    setSelectedSuiteId(null); //clearing suite field if location changes after selecting suite
    setDates(null); //clearing date filed if location changes after selecting dates
  }, [selectedLocationId]);

  //this useEffect will fetch avilable beds from backend after getting suiteId and dates

  useEffect(() => {
    setSelectedBedsId(null); //clearing bed field if date or suite id changes after selecting bed field
    setAdultCount(null); //clearing adultCount field if date or suite id changes after selecting adult field
    setChildCount(null); //clearing childCount field to if date or suite id changes after selecting child field

    if (selectedSuiteId === null) {
      setBedOptions([]); //clearing bed options if selected suite id gets null
      setDates(null);
    }

    //if suite id and date field are available this section will look in the backend for avilable beds on the suite in these days

    if (selectedSuiteId && dates) {
      setBedLoading(true);

      // sending the post request for searching avilable dates
      AxiosConfig.post("/api/v1/search/beds", {
        startDate: dates[0],
        endDate: dates[1],
        suiteIds: [selectedSuiteId?.value],
      })
        .then((response) => {
          // filtering the avilable beds from data given by the backend

          filterBedOptions(response.data.results);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [dates, selectedSuiteId]);

  useEffect(() => {
    setDates(null);
    setBedOptions([]); //clearing bed options if selected suite id gets null
  }, [selectedSuiteId]);

  //this useEffect will set the disble button by checking the required field's avilability
  //It also updates if any field changes.

  useEffect(() => {
    // setting amount based on the selected beds

    if (selectedBedsId === null) {
      setAmount(0);
    } else {
      setAmount(calculateAmount());
    }

    //button disability control based on the required fields availability

    if (
      selectedSuiteId?.value &&
      selectedBedsId &&
      dates &&
      adultCount &&
      childCount
    ) {
      setButtonDisable(false);
    } else {
      setButtonDisable(true);
    }
  }, [
    selectedLocationId,
    selectedSuiteId,
    selectedBedsId,
    dates,
    adultCount,
    childCount,
  ]);

  useEffect(() => {
    let temp = localStorage.getItem("atlanta_token");
    setToken(temp);
  }, []);

  // function for filtering location options from  /api/v1/location

  const filterLocationOptions = (data) => {
    const locations = [];
    data.map((item) => {
      locations.push({ value: item.id, label: item.name });
    });
    setLocationOptions(locations);
    setLocationLoading(false);
  };

  // function for filtering property options from  /api/v1/location

  const filterPropertyOptions = () => {
    const properties = [];
    const locationDetails = apiData?.find(
      (item) => item.id === selectedLocationId?.value
    );
    locationDetails?.properties.map((item) => {
      properties.push({ value: item.id, label: item.name });
    });

    return properties;
  };

  // function for filtering suite options from  /api/v1/location

  const filterSuiteOptions = () => {
    const suites = [];
    const locationDetails = apiData?.find(
      (item) => item.id === selectedLocationId?.value
    );

    const propertyDetails =
      locationDetails?.properties[selectedPropertyId?.value - 1];

    propertyDetails?.suites.map((item) => {
      suites.push({ value: item.id, label: item.name });
    });

    return suites;
  };

  // function for filtering bed options from api data from /api/v1/search/beds

  const filterBedOptions = (data) => {
    const beds = [];
    data.map((item) => {
      beds.push({ value: item.id, label: item.name, price: item.price });
    });
    setBedOptions(beds);
    setBedLoading(false);
  };

  // function for any change in the bed field

  const handleBedFieldChange = (e, { action }) => {
    action !== "clear" && setSelectedBedsId(e);
    action === "clear" && setSelectedBedsId(null);
  };

  const handleBooking = async () => {
    const token = JSON.parse(localStorage.getItem("atlanta_token"));

    const formatBedIds = () => {
      const bedIds = [];
      selectedBedsId.map((item) => {
        bedIds.push(item.value);
      });
      return bedIds;
    };

    const info = {
      startDate: dates[0],
      endDate: dates[1],
      locationId:selectedLocationId,
      propertyId:selectedPropertyId,
      suiteId:selectedSuiteId,
      bedIds: formatBedIds(),
      adultCount: adultCount.value,
      childCount: childCount.value,
    };

    localStorage.setItem("booking-info", JSON.stringify(info));

    token
      ? router.push("/payment")
      : router.push({
          pathname: "/login-signup",
          query: { next: "payment" },
        });
  };

  //function for calculating price

  const calculateAmount = () => {
    let price = 0;
    selectedBedsId.map((item) => {
      price = price + item.price;
    });

    return price;
  };

  return (
    <div>
      {/* Location field */}

      <Select
        isLoading={locationLoading}
        placeholder={locationLoading ? "loading options..." : "Select Location"}
        id="select-location"
        instanceId="select-location"
        options={locationOptions}
        onChange={(e) => setSelectedLocationId(e)}
      />

      {/* Property field */}

      <Select
        value={selectedPropertyId}
        className="mt-5"
        isLoading={locationLoading}
        placeholder={locationLoading ? "loading options..." : "Select Property"}
        id="select-property"
        instanceId="select-property"
        options={filterPropertyOptions()}
        onChange={(e, { action }) => {
          action === "clear"
            ? setSelectedPropertyId(null)
            : setSelectedPropertyId(e);
        }}
        noOptionsMessage={() => {
          return selectedLocationId?.value
            ? "No property in this location"
            : "Please select location first";
        }}
        isClearable
      />

      {/* Suite field */}

      <Select
        value={selectedSuiteId}
        className="mt-5"
        isLoading={locationLoading}
        placeholder={locationLoading ? "loading options..." : "Select Suite"}
        id="select-suite"
        instanceId="select-suite"
        options={filterSuiteOptions()}
        onChange={(e, { action }) => {
          action !== "clear" && setSelectedSuiteId(e);
          action === "clear" && setSelectedSuiteId(null);
        }}
        noOptionsMessage={() => {
          return selectedPropertyId?.value
            ? "No Suites in this property"
            : "Please select Property first";
        }}
        isClearable
      />

      {/* Checkin checkout date field */}

      <DatePicker
        setDates={setDates}
        setBedLoading={setBedLoading}
        locationId={selectedLocationId?.value}
        suiteId={selectedSuiteId?.value}
      />

      {/* Bed field */}

      <Select
        className="mt-5"
        value={selectedBedsId}
        isLoading={bedLoading}
        placeholder={bedLoading ? "loading options..." : "Select Bed"}
        id="select-bed"
        instanceId="select-bed"
        options={selectedSuiteId?.value ? bedOptions : []}
        noOptionsMessage={() => {
          return selectedSuiteId?.value && dates
            ? "Sorry! No Bed available right now"
            : "Please select above fields";
        }}
        isMulti
        onChange={handleBedFieldChange}
      />

      {/* Adult field */}

      <Select
        className="mt-5"
        value={adultCount}
        placeholder="Select number of adults(s)"
        id="select-adult"
        instanceId="select-adult"
        options={
          selectedSuiteId?.value && dates && selectedBedsId
            ? adultOptionsGenerator(selectedSuiteId?.value)
            : []
        }
        noOptionsMessage={() => {
          return selectedSuiteId?.value && dates && selectedBedsId
            ? null
            : "Please select avobe fields";
        }}
        onChange={(e) => {
          setAdultCount(e);
        }}
      />

      {/* children field */}

      <Select
        className="mt-5"
        value={childCount}
        id="select-child"
        instanceId="select-child"
        placeholder="Select number of child(ren)"
        options={
          selectedSuiteId?.value && dates && selectedBedsId
            ? childOptionsGenerator(selectedSuiteId?.value)
            : []
        }
        noOptionsMessage={() => {
          return selectedSuiteId?.value && dates && selectedBedsId
            ? null
            : "Please select avobe fields";
        }}
        onChange={(e) => {
          setChildCount(e);
        }}
      />

      <div className="grid md:grid-cols-2 mt-5 pl-2 font-extrabold">
        <p className="text-white font-extrabold font-primary md:text-lg">
          Accommodation Cost:{amount.toFixed(2)}
        </p>
      </div>

      {/* request to book button */}

      <button
        className={`${
          buttonDisable ? "bg-gray-300 text-gray-400" : "bg-yellow-primary"
        } text-gray-500 font-primary font-bold w-full h-12 mt-5`}
        onClick={() => handleBooking()}
        disabled={buttonDisable}
      >
        Request to book
      </button>

      {/* warning about button disability */}

      {buttonDisable && (
        <p className="text-white mt-2">
          *Book button is disabled. Please fill above form to make it Enabled
        </p>
      )}
      <Toaster />
    </div>
  );
};

export default index;
