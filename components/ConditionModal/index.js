import React from "react";
import { useDispatch } from "react-redux";
import { decline } from "../../redux/slices/authSlice";
import { toggleModalState } from "../../redux/slices/modalSlice";
import { useRouter } from "next/router";

const index = ({next}) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const handleDecline = () => {
    dispatch(toggleModalState());
    dispatch(decline());
  };

  const handleAccepted = () => {
    dispatch(toggleModalState());
    router.push(`/${next || ""}`);
  };

  return (
    <div className="z-50 px-5">
      <p className="text-center text-primary md:text-xl">
        Welcome to Atlanta's shared living
      </p>
      <p className="text-center md:text-lg py-3">Terms and Conditions</p>
      <p className="pb-3">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam
        eveniet, illo dolores delectus voluptatum eum voluptate molestiae sint
        assumenda nam quidem a temporibus incidunt
      </p>

      <p className="pb-3">
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias
        maiores id autem, voluptas consequuntur nam recusandae expedita natus
        rem totam ipsa fugiat dolore alias enim cumque officiis minima. Ipsa,
        provident!
      </p>

      <button
        className="w-full bg-yellow-primary text-gray-600 h-10 rounded-md"
        onClick={() => handleAccepted(true)}
      >
        Agree and continue
      </button>
      <button
        onClick={() => handleDecline()}
        className="w-full text-center border-2 h-10 rounded-md"
      >
        Decline
      </button>
    </div>
  );
};

export default index;
