import TabButton from "./Buttons/TabButton";
import HeroCard from "./Cards/HeroCard";
import LegendCard from "./Cards/LegendCard";
import HeroCardWithChildren from "./Cards/HeroCardWithChildren";
import BookNowCard from "./Cards/BookNowCard";
import ImageSlider from "./ImageContainer/ImageSlider";
import SuitCard from "./Cards/SuitCard";
import SuitCardDetailed from "./Cards/SuitCardDetailed";
import TabCard from "./Cards/TabCard";
import AgentImageContainer from "./ImageContainer/AgentImageContainer";
import PriceText from "./Paragraph/SuitText/PriceText";
import FacilityText from "./Paragraph/SuitText/FacilityText";
import BookNowSuitImage from './ImageContainer/BookNowSuitImage';
import SuitImage from "./ImageContainer/SuitImage";
import SingleSuitSlider from './Sliders/SingleSuitSlider'
import TabCard2 from "./Cards/TabCard/TabCard2";
import StarRating from './StarRating'
import ReviewCard from "./Cards/ReviewCard";


// export all components form here
export {
    HeroCard,
    LegendCard,
    TabButton,
    TabCard,
    HeroCardWithChildren,
    BookNowCard,
    AgentImageContainer,
    SuitCard,
    SuitCardDetailed,
    PriceText,
    FacilityText,
    BookNowSuitImage,
    SuitImage,
    ImageSlider,
    SingleSuitSlider,
    TabCard2,
    StarRating,
    ReviewCard
};
