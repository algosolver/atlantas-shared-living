import { useEffect } from "react";
import style from "./style.module.css";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useDispatch } from "react-redux";
import ModalWrapper from "../../Modal/ModalWrapper";
import { toggleModalState } from "../../../redux/slices/modalSlice";
import { signUp, addImages } from "../../../redux/slices/authSlice";
import ConditionModal from "../../ConditionModal";
import CreateProfile from "../CreateProfile";
import AxiosConfig from "../../../AxiosConfig/AxiosConfig";
import { toast } from "react-toastify";
import Toaster from "../../Toaster";
import moment from "moment";
import { useRouter } from "next/router";

const index = ({query}) => {
  const [passwordFieldType, setPasswordFieldType] = useState("password");
  const [accepted, setAccepted] = useState(false);
  const [selectDl, setSelectDl] = useState(null);
  const [selectPi, setSelectPi] = useState(null);
  const [dlpreview, setDlPreview] = useState();
  const [piPreview, setPiPreview] = useState();
  const [buttonDisable, setButtonDisable] = useState(true);

  const router = useRouter();

  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitSuccessful },
    reset,
  } = useForm();

  const onSubmit = async (data) => {
    if (data) {
      const formData = new FormData();
      formData.append("firstName", data.firstName);
      formData.append("lastName", data.lastName);
      formData.append("dob", moment(data.dob).toISOString());
      formData.append("phone", data.phone);
      formData.append("email", data.email);
      formData.append("password", data.password);
      formData.append("image", data.image[0]);
      formData.append("drivingLicense", data.drivingLicense[0]);
      const res = await signUpHandler(formData);
      if (res.statusCode === 201) {
        dispatch(signUp(data));
        dispatch(addImages({ selectDl, selectPi }));
        dispatch(toggleModalState());
        setDlPreview(null);
        setPiPreview(null);
        setButtonDisable(true);
        localStorage.setItem(
          "atlanta_token",
          JSON.stringify(res?.results?.token)
        );
        reset();
      } else if (res.response.statusCode === 400) {
        toast(res?.response?.errors?.email);
        toast(res?.response?.errors?.password);
      } else {
        toast("something went wrong, please try again!");
      }
    }
  };

  const signUpHandler = async (data) => {
    try {
      const result = await AxiosConfig.post(`api/v1/auth/signup`, data);
      console.log(result);
      return result?.data;
    } catch (error) {
      console.log(error);
      return {
        success: false,
        status: error.response?.status,
        response: error.response?.data,
      };
    }
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = () => {
        reject(error);
      };
    });
  };

  const onSelectDl = async (e) => {
    const base64 = await convertBase64(e.target.files[0]);
    if (base64 instanceof Error) {
      console.log("Error: ", result.message);
      return;
    }
    setSelectDl(base64);
    setDlPreview(base64);
  };

  const onSelectPi = async (e) => {
    const base64 = await convertBase64(e.target.files[0]);
    if (base64 instanceof Error) {
      console.log("Error: ", result.message);
      return;
    }
    setSelectPi(base64);
    setPiPreview(base64);
  };

  const handlePasswordFieldType = () => {
    passwordFieldType === "password"
      ? setPasswordFieldType("text")
      : setPasswordFieldType("password");
  };

  useEffect(() => {
    if (selectDl && selectPi) {
      setButtonDisable(false);
    }
  }, [selectDl, selectPi]);

  return (
    <div className={style["signUpForm-wrapper"]}>
      <p className="text-base lg:text-2xl font-bold text-center text-primary">
        Welcome to Atlanta's Shared Living
      </p>
      <p className="text-lg text-center mt-5">Sign Up</p>

      <form
        name="signUpForm"
        onSubmit={handleSubmit(onSubmit)}
        className="grid grid-cols-2 gap-x-4"
      >
        <div className="col-span-2 mt-4">
          <input
            type="text"
            placeholder="First Name"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("firstName", { required: true })}
          />

          {errors.firstName?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}

          <p className="mt-2 ml-2 text-gray-600">
            First Name should match Government Id
          </p>
        </div>

        <div className="col-span-2 mt-4">
          <input
            type="text"
            placeholder="Last Name"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("lastName", { required: true })}
          />
          {errors.lastName?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}
          <p className="mt-2 ml-2 text-gray-600">
            Last Name should match Government Id
          </p>
        </div>

        <div className="col-span-2 mt-4">
          <input
            type="date"
            placeholder="Birthdate"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("dob", { required: true })}
          />

          {errors.dob?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}

          <p className="mt-2 ml-2 text-gray-600">
            You need to be at least 18 years old. (Format: MM/DD/YYYY)
          </p>
        </div>

        <div className="col-span-2 mt-4">
          <input
            type="tel"
            placeholder="Phone Number"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("phone", {
              required: true,
              pattern:
                /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
            })}
          />

          {errors.phone?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}

          {errors.phone?.type === "pattern" && (
            <span className="text-red-400">
              Please enter a valid phone number
            </span>
          )}

          <p className="mt-2 ml-2 text-gray-600">
            We'll contact you directly if necessary. (Format: 1-123-456-7890)
          </p>
        </div>

        <div className="col-span-2 mt-4">
          <input
            type="text"
            placeholder="Email"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("email", {
              required: true,
              pattern:
                /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
            })}
          />
          {errors.email?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}
          {errors.email?.type === "pattern" && (
            <span className="text-red-400">Please enter a valid email</span>
          )}

          <p className="mt-2 ml-2 text-gray-600">
            We'll email you confirmation receipts
          </p>
        </div>

        <div className="col-span-2 mt-4 relative">
          <input
            type={passwordFieldType}
            placeholder="Password"
            className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
            {...register("password", {
              required: true,
              minLength: 8,
              pattern: /(?:[A-Z].*[0-9])|(?:[0-9].*[A-Z])/,
            })}
          />

          <p
            className="absolute top-4 right-4 underline cursor-pointer"
            onClick={() => handlePasswordFieldType()}
          >
            {passwordFieldType === "password" ? "show" : "hide"}
          </p>
          {errors.password?.type === "required" && (
            <span className="text-red-400">This field is required</span>
          )}
          {errors.password?.type === "minLength" && (
            <span className="text-red-400">
              Password must contain at least 8 character
            </span>
          )}
          {errors.password?.type === "pattern" && (
            <span className="text-red-400">
              Password must contain at least one upper case letter and one digit
            </span>
          )}

          <p className="mt-2 ml-2 text-gray-600">
            Password should be at least 8 character long and contain at least
            one capital letter and one digit.
          </p>
        </div>

        {/* driving license upload field */}

        <div className="col-span-2">
          <input
            accept="image/*"
            id="dl-image-upload"
            type="file"
            hidden
            {...register("image")}
            onChange={onSelectDl}
          />
          <div className="border-2 h-72 grid place-items-center p-4 mb-5 mt-5">
            {dlpreview ? (
              <img src={dlpreview} className={"h-60"} />
            ) : (
              <img
                src="https://cdn4.iconfinder.com/data/icons/office-thick-outline/36/office-14-128.png"
                alt=""
              />
            )}
          </div>
          <label
            htmlFor="dl-image-upload"
            className={`${style["signUpForm-dl-upload-label"]}`}
          >
            <span className={"cursor-pointer sm:text-lg text-white font-bold"}>
              Upload Id (passport/driving license)
            </span>
          </label>
        </div>

        {/* Profile image upload field */}

        <div className="col-span-2">
          <input
            accept="image/*"
            id="profile-image-upload"
            type="file"
            hidden
            {...register("drivingLicense")}
            onChange={onSelectPi}
          />
          <div className="border-2 h-72 grid place-items-center p-4 mb-5 mt-5">
            {piPreview ? (
              <img src={piPreview} className={"h-60"} />
            ) : (
              <img
                src="https://cdn4.iconfinder.com/data/icons/office-thick-outline/36/office-14-128.png"
                alt=""
              />
            )}
          </div>
          <label
            htmlFor="profile-image-upload"
            className={`${style["signUpForm-dl-upload-label"]}`}
          >
            <span className={"cursor-pointer sm:text-lg text-white font-bold"}>
              Upload Your Profile Image
            </span>
          </label>
        </div>

        <button
          className={`${style["signUpForm-submit-btn"]} ${
            buttonDisable
              ? "bg-gray-400 pointer-events-none"
              : "bg-yellow-primary"
          }`}
          type="submit"
          disabled={buttonDisable}
        >
          Continue
        </button>

        {buttonDisable && (
          <p className="col-span-2 mt-2 ml-2 text-red-400">
            Button is disabled. To continue you must upload photo of your id
          </p>
        )}
      </form>
      <Toaster />
      <ModalWrapper>
        <ConditionModal setAccepted={setAccepted} next={query.next}/>
      </ModalWrapper>
    </div>
  );
};

export default index;
