import style from "./style.module.css";
import { useForm } from "react-hook-form";
import { FaFacebookF } from "react-icons/fa";
import { FcGoogle } from "react-icons/fc";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { signIn } from "../../../redux/slices/authSlice";
import AxiosConfig from "../../../AxiosConfig/AxiosConfig";
import { toast } from "react-toastify";
import Toaster from "../../Toaster";

const index = ({query}) => {

  const [email, setEmail] = useState('');
  useEffect(()=>{
    setEmail(localStorage.getItem('email'));
  },[])
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const router = useRouter();
    const dispatch = useDispatch();

    const onSubmit = async (data) => {
        // console.log('in login query', query);
        // data ? console.log(data) : null;
        const res = await signinHandler(data);
      
        if(res?.statusCode===200 && res?.results){
          dispatch(signIn(res?.results?.user));
          if(res?.results?.token){
            console.log(res.results);
            localStorage.setItem(
              "atlanta_token",
              JSON.stringify(res?.results?.token)
              
            );
            localStorage.removeItem('email');
            router.push(`/${query?.next || ''}`);
          }
        }
        else{
          toast("Wrong Email or password!")
        }
        
    };

    const signinHandler = async (data) => {
      try {
        const {password} = data;
        const result = await AxiosConfig.post(`api/v1/auth/login`, {email, password});
        return result?.data;
      } catch (error) {
        console.log(error);
      }
    };

    return (
      <div className={style["contactUsForm-wrapper"]}>
        <p className="text-base lg:text-2xl font-bold text-center text-primary">
          Welcome to Atlanta's Shared Living
        </p>
        <p className="text-lg text-center mt-5">Log in</p>

        <form
          name="contactUsForm"
          onSubmit={handleSubmit(onSubmit)}
          className="grid grid-cols-2 gap-x-4"
        >
          <div className="col-span-2 mt-4">
            <label htmlFor="email" className="text-lg">
              Email address
            </label>
            <input
              type="text"
              id="email"
              value={email || ''}
              readOnly={true}
              // placeholder="email"
              className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
              // {...register("email", {
              //   required: true,
              //   pattern:
              //     /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
              // })}
            />
            {/* {errors.email?.type === "required" && (
              <span className="text-red-400">This field is required</span>
            )}
            {errors.email?.type === "pattern" && (
              <span className="text-red-400">Please enter a valid email</span>
            )} */}
          </div>

          <div className="col-span-2 mt-4">
            <label htmlFor="password" className="text-lg">
              Password
            </label>
            <input
              type="password"
              id="password"
              placeholder="password"
              className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
              {...register("password", {
                required: true,
                minLength: 6,
                maxLength: 20
              })}
            />
            {errors.password?.type === "required" && (
              <span className="text-red-400">This field is required</span>
            )}
            {errors.password?.type === "minLength" && (
              <span className="text-red-400">Password must be minimum 6 characters long</span>
            )}
            {errors.password?.type === "maxLength" && (
              <span className="text-red-400">Password must be maximum 20 characters long</span>
            )}
          </div>

          <button className={style["contactUsForm-submit-btn"]} type="submit">
            Continue
          </button>
        </form>
        <Toaster />
      </div>
    );
};

export default index;
