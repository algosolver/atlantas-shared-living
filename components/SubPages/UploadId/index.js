import React from "react";
import BackButton from "./BackButton";
import ImageUpload from "./ImageUpload";

const UploadId = () => {
  return (
    <>
      <BackButton />
      <div className={"text-center"}>
        <h1 className={"text-sm font-bold pb-2 border-b"}>
          Please upload photo of your Id
        </h1>
        <p className={"text-xs font-bold pt-4 border-t border-gray-900"}></p>
        <div className={"px-6 "}>
          <h1 className={"text-xs font-medium pb-2 text-gray-600 border-b "}>
            Please upload your driver license or passport photo to verify your
            identity.
          </h1>
          <ImageUpload />
        </div>
      </div>
    </>
  );
};

export default UploadId;
