import React from "react";
import BackButton from "./BackButton";
import ImageUpload from "./ImageUpload";

const CreateProfile = () => {
  return (
    <>
      <BackButton />
      <div className={"text-center"}>
        <h1 className={"text-sm font-bold pb-2 border-b"}>
          Create Your Profile
        </h1>
        <p className={"text-xs font-bold pt-4 border-t border-gray-900"}>
          step 1 of 4
        </p>
        <div className={"px-6"}>
          <h1 className={"text-sm font-bold my-2 border-b"}>
            Add a Profile Photo
          </h1>
          <h1 className={"text-xs font-medium pb-2 text-gray-600 border-b "}>
            Pick an image that shows your face. Hosts won't be able to see your
            profile photo until your reservation is confirmed{" "}
          </h1>
          <ImageUpload />
        </div>
      </div>
    </>
  );
};

export default CreateProfile;
