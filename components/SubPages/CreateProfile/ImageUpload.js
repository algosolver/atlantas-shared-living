import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addProfileImage,decline } from "../../../redux/slices/authSlice";
import { toggleModalState } from "../../../redux/slices/modalSlice";

const ImageUpload = () => {
  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();
  const dispatch = useDispatch();

  // useEffect(() => {
  //   if (!selectedFile) {
  //     setPreview(undefined);
  //     return;
  //   }

  //   const objectUrl = URL.createObjectURL(selectedFile);
  //   setPreview(objectUrl);

  //   return () => URL.revokeObjectURL(objectUrl);
  // }, [selectedFile]);

  const handleDecline = () =>{
    dispatch(decline());
    dispatch(toggleModalState());
  }

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const onSelectFile = async (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }
    setSelectedFile(e.target.files[0]);
    const result = await toBase64(e.target.files[0]).catch((e) => Error(e));
    if (result instanceof Error) {
      console.log("Error: ", result.message);
      return;
    }

    setPreview(selectedFile);
    // store this result to redux

  };

  return (
    <div>
      <input
        accept="image/*"
        id="avatar-image-upload"
        type="file"
        hidden
        onChange={onSelectFile}
      />
      <div>
        {preview ? (
          <img
            src={preview}
            className={"mx-auto my-4"}
            style={{ width: "200px", borderRadius: "200px" }}
          />
        ) : (
          <img
            src="https://cdn4.iconfinder.com/data/icons/office-thick-outline/36/office-14-128.png"
            alt=""
            className="mx-auto w-36 my-4"
          />
        )}
      </div>
      <label
        htmlFor="avatar-image-upload"
        className={
          "relative inline-block w-full h-full py-1 bg-black cursor-pointer rounded"
        }
      >
        <span className={"cursor-pointer text-sm text-white"}>
          Upload Photo
        </span>
      </label>
      <button
        className={
          "w-full mt-3 rounded py-1 text-sm bg-white text-black border-2 border-black"
        }
        onClick={()=>handleDecline()}
      >
        Decline
      </button>
    </div>
  );
};

export default ImageUpload;
