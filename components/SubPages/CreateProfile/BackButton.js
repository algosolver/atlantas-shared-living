import React from "react";
import { FaAngleLeft } from "react-icons/fa";

const BackButton = () => {
  return (
    <button className={"absolute pl-4 pt-1 focus:outline-none"}>
      <FaAngleLeft className={"pl-2 font-normal text-gray-600"} />
    </button>
  );
};

export default BackButton;
