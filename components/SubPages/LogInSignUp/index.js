import style from "./style.module.css";
import { useForm } from "react-hook-form";
import { FaFacebookF } from "react-icons/fa";
import { FcGoogle } from "react-icons/fc";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { emailSubmission } from "../../../redux/slices/authSlice";
import AxiosConfig from "../../../AxiosConfig/AxiosConfig";
import { toast } from "react-toastify";
import Toaster from "../../Toaster";

const index = ({query}) => {

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const router = useRouter();
  const dispatch = useDispatch();

  const onSubmit = async (data) => {
    // data ? console.log(data) : null;
    const res = await checkEmail(data);
    // console.log('data', data);
    // console.log("ressss", res);
    if (res.statusCode === 200) {
      localStorage.setItem('email', data.email);
      router.push({
        pathname: '/login',
        query: { next: query.next },
      });
      
    } else if (res.response.statusCode === 400) {
      dispatch(emailSubmission());
      router.push({
        pathname: '/signup',
        query: { next: query.next },
      });
    } else {
      toast("something went wrong, please try again!");
    }
  };

  const checkEmail = async (email) => {
    try {
      const result = await AxiosConfig.post(`api/v1/auth/check-email`, email);
      return result?.data;
    } catch (error) {
      return {
        success: false,
        status: error.response.status,
        response: error.response.data,
      };
    }
  };
    return (
      <div className={style["contactUsForm-wrapper"]}>
        <p className="text-base lg:text-2xl font-bold text-center text-primary">
          Welcome to Atlanta's Shared Living
        </p>
        <p className="text-lg text-center mt-5">Log in or sign up</p>

        <form
          name="contactUsForm"
          onSubmit={handleSubmit(onSubmit)}
          className="grid grid-cols-2 gap-x-4"
        >
          <div className="col-span-2 mt-4">
            <label htmlFor="email" className="text-lg">
              Email address
            </label>
            <input
              type="text"
              id="email"
              placeholder="email"
              className="w-full h-12 focus:outline-none rounded-md p-4 mt-1 border-2"
              {...register("email", {
                required: true,
                pattern:
                  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
              })}
            />
            {errors.email?.type === "required" && (
              <span className="text-red-400">This field is required</span>
            )}
            {errors.email?.type === "pattern" && (
              <span className="text-red-400">Please enter a valid email</span>
            )}
          </div>

          <button className={style["contactUsForm-submit-btn"]} type="submit">
            Continue
          </button>
        </form>
        <Toaster />
      </div>
    );
};

export default index;
