import React from "react";
import Divider from "../RequestToBook/Divider";
import Details from "./Details";
import RoomSummary from "./RoomSummary";

const Summary = ({amount}) => {

  

  return (
    <div className={"mt-5"}>
      <div className="border px-5 py-3 rounded-md ">
        <p className="font-bold mb-5">Price Details</p>
        <Details title="Accommodation" amount={amount} totalComponent={false} />
        <Details title="Service fee" amount={100.00} totalComponent={false} />
        <Details title="Total Pay" amount={amount+100.00} totalComponent={true} />
      </div>
    </div>
  );
};

export default Summary;
