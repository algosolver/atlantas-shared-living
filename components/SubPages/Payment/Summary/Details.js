import React from "react";

const Details = ({ title, amount, totalComponent }) => {
  return (
    <div className={"flex justify-between items-center my-2"}>
      <div>
        <p
          className={`text-xs md:text-sm ${
            totalComponent
              ? "font-bold text-gray-700"
              : "font-medium text-gray-500"
          }   underline`}
        >
          {title}
        </p>
      </div>
      <div>
        <p
          className={`text-xs md:text-sm ${
            totalComponent
              ? "font-bold text-gray-700"
              : "font-medium text-gray-500"
          }  `}
        >
          ${amount}
        </p>
      </div>
    </div>
  );
};

export default Details;
