import React from "react";
import { AiFillStar } from "react-icons/ai";
import { BiMedal } from "react-icons/bi";
const RoomSummary = () => {
  return (
    <div className=" mx-auto max-w-sm  overflow-hidden">
      <div className="sm:flex sm:items-start">
        <img
          className="block h-16 sm:h-24 rounded-xl mx-auto mb-4 sm:mb-0 sm:mr-4 sm:ml-0"
          src="https://cdn1.iconfinder.com/data/icons/work-from-home-25/512/WorkFromHome_sleep-night-bedroom-bed-128.png"
          alt=""
        />
        <div className="text-center sm:text-left sm:flex-grow">
          <div className="mb-4">
            <p className="text-xs font-semibold text-gray-500 mb-2">
              Lorem ipsum dolor
            </p>
            <p className="text-base text-grey-600">Lorem ipsum dolor sit.</p>
            <p className="text-xs font-semibold text-gray-500 mb-2">
              Lorem ipsum dolor
            </p>
            <div className={"flex items-center justify-center"}>
              <AiFillStar className={"text-xs text-pink-600 mr-1"} />
              <p className={"text-xs mr-1"}>4.96(57 reviews)</p>
              <BiMedal className={"text-xs text-pink-600 mr-1"} />
              <p className={"text-xs mr-1"}>Superhost</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RoomSummary;
