import React,{useState} from "react";
import Divider from "./Divider";
import Pay from "./Pay";
import Required from "./Required";
import Cancellation from "./Cancellation";
import Reminder from "./Reminder";
import ReqFooter from "./ReqFooter";
import { PaymentSuccessModal } from "../../../PaymentSuccessModal";

const RequestToBook = ({amount}) => {

  return (
    <div className={"px-2 md:px-6"}>
      <Divider />
      <Required />
      <Divider />
      <Cancellation />
      <Divider />
      <Reminder />
      <Divider />
      <Pay amount={amount} />
      <ReqFooter />
      
    </div>
  );
};

export default RequestToBook;
