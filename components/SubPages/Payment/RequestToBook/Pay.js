import React from "react";
import StripeContainer from "../../../../components/Payment/Stripe/StripeContainer"

const Pay = ({amount}) => {
  return (
    <div className="my-2">
      <p className="font-bold mb-3">Enter your Card information here</p>
      <div className={"border"}>
        <StripeContainer amount={amount}/>
      </div>
    </div>
  );
};

export default Pay;
