import React from "react";
import { AiOutlinePlus } from "react-icons/ai";
const CardContainer = ({ icon, text }) => {
  return (
    <div className={"border-b"}>
      <div className={"flex justify-between items-center mx-4 my-2"}>
        <div className={"flex items-center text-gray-500"}>
          {icon}
          <p className="text-xs md:text-sm ml-2">{text}</p>
        </div>
        <AiOutlinePlus className={"text-gray-500 cursor-pointer"} />
      </div>
    </div>
  );
};

export default CardContainer;
