import React from "react";

const Reminder = () => {
  return (
    <div className={"flex justify-between items-center my-2"}>
      <div className={"flex items-center text-gray-500 mr-4"}>
        <img
          src="https://cdn4.iconfinder.com/data/icons/office-business-vol-3-part-1-1/512/event_calendar-64.png"
          alt="reminder"
        />
      </div>
      <div>
        <p className="text-xs font-bold text-gray-800">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas cum
          quae voluptatem doloribus.
        </p>
        <p className="text-xs font-medium text-gray-400">
          Lorem ipsum dolor sit amet consectetur.
        </p>
      </div>
    </div>
  );
};

export default Reminder;
