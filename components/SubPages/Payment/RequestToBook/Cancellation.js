import React from "react";

const Cancellation = () => {
  return (
    <div className="my-2">
      <p className="font-bold mb-3">Cancellation policy</p>
      <p className="text-xs font-medium text-gray-400">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas cum
        quae voluptatem doloribus laborum veritatis quisquam voluptates aliquam
        dignissimos.
      </p>
      <p className="text-xs font-medium text-gray-800 underline cursor-pointer">
        Learn more
      </p>
      <p className="text-xs font-medium text-gray-400 mt-4">
        <span className="text-xs font-medium text-gray-800 mr-1">
          Lorem ipsum dolor sit amet consectetur
        </span>
        adipisicing elit. Voluptas cum quae voluptatem doloribus laborum
        veritatis quisquam voluptates aliquam dignissimos. adipisicing elit.
        Voluptas cum quae voluptatem doloribus laborum veritatis quisquam
        voluptates aliquam dignissimos.
      </p>
    </div>
  );
};

export default Cancellation;
