import React from "react";

const ReqFooter = () => {
  return (
    <>
      <p className="text-xs font-medium text-gray-400 mt-4">
        adipisicing elit. Voluptas
        <span className="text-xs font-medium text-blue-700 mx-1 underline">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas cum
        </span>
        quae voluptatem doloribus laborum veritatis quisquam voluptates aliquam
        dignissimos.
      </p>
    </>
  );
};

export default ReqFooter;
