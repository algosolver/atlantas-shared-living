import React from "react";

const Divider = () => {
  return (
    <div
      style={{ height: "0.8px" }}
      className={"w-full bg-gray-300 my-6"}
    ></div>
  );
};

export default Divider;
