import React from "react";

const Required = () => {
  return (
    <div className="my-2">
      <p className="font-bold mb-3">Required For your Trip</p>

      <div className={"flex justify-between items-start my-2"}>
        <div className={" text-gray-500"}>
          <p className="text-xs md:text-sm text-gray-700 font-bold">
            Message the host
          </p>
          <p className="text-xs font-medium">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.{" "}
          </p>
        </div>
        <div>
          <button
            className={
              "border border-black font-bold px-3 py-1 rounded-lg text-xs"
            }
          >
            Add
          </button>
        </div>
      </div>
    </div>
  );
};

export default Required;
