import React from 'react'
import BookingForm from "../../BookingForm"

const index = () => {
  return (
    <div className="lg:mt-44 bg-primary px-5 md:px-20 lg:px-72 py-10 md:py-20">
      <BookingForm/>
    </div>
  )
}

export default index
