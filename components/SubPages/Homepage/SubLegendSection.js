import React from "react";
import { LegendCard } from "../../";
const title =
  "AVOID OVERPRICED HOTELS AND OPTIMIZE YOUR STAY AT ASLTHINC.";
const desc = `At ASLTHinc. we've created the perfect living solution, customized for those in transition. We provide an upscale, safer, flexible, non-traditional way of living.`;

const SubLegendSection = () => {
  return (
    <div>
      <LegendCard
        legendImg={'./Images/logo-favicon-2.webp'}
        title={title}
        desc={desc}
        classProps={"text-2xl md:text-4xl font-extralight"}
      />
    </div>
  );
};

export default SubLegendSection;
