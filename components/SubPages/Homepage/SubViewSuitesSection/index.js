import React from "react";
import style from "./SubViewSuitesSection.module.css";
import { FaLongArrowAltRight } from "react-icons/fa";
import "react-dropdown/style.css";
import { useRouter } from "next/router";
import BookingForm from "../../../BookingForm"


const SubViewSuitesSection = () => {
  const router = useRouter();

  return (
    <div className={style["SubViewSuites-wrapper"]}>
      {/* content section of the component */}

      <div className={style["SubViewSuites-content-wrapper"]}>
        <p className={style["SubViewSuites-content-text"]}>
          Our extended stay/transitional housing booking options are: nightly,
          weekly, bi-weekly, and month to month. We accept all supportive
          payment programs.
        </p>
        <button
          onClick={() => router.push("/our-suites")}
          className={style["SubViewSuites-content-button"]}
        >
          view our suites <FaLongArrowAltRight className="inline-block" />
        </button>
      </div> 

       {/* Form section of the component */}

      <div className={style["SubViewSuites-form-wrapper"]}>
        <BookingForm/>
      </div>
    </div>
  );
};

export default SubViewSuitesSection;
